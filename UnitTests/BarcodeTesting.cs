﻿using OpenAir.Barcode;

namespace UnitTests
{
    public class BarcodeTesting
    {
        [SetUp]
        public void Setup()
        {
            
        }
        
        [Test]
        public void TestValid()
        {
            Assert.True(Codec.ValidCode("FV12O23"));
            Assert.True(Codec.ValidCode("qh01u31"));
            
            Assert.False(Codec.ValidCode("ab00o01")); // 00 is no date
            Assert.False(Codec.ValidCode("ab32o01")); // 32 is no date
            
            Assert.False(Codec.ValidCode("a330o01")); // a3
            Assert.False(Codec.ValidCode("ab16z01")); // z
            Assert.False(Codec.ValidCode("ab12u012")); // 012
            Assert.False(Codec.ValidCode("abz12u01")); // abz
        }
    }
}