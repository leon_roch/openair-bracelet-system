﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace OpenAir.Models
{
    public abstract class BaseZone
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public char Code { get; set; }
        [Required]
        public string Name { get; set; }
    }
}