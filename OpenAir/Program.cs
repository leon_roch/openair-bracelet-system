using System;
using Microsoft.AspNetCore.Builder;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using OpenAir.Data;
using OpenAir.Services;


var builder = WebApplication.CreateBuilder(args);

// ---- Add services to the container. ---- \\

// Add DB Context (https://github.com/PomeloFoundation/Pomelo.EntityFrameworkCore.MySql)
var vers = new MariaDbServerVersion(new Version(10, 6, 4)); // version is very important

builder.Services.AddDbContext<ZoneContext>(options => 
    options.UseMySql(
        "Server=127.0.0.1;Database=openair;Uid=root;Pwd=s3cureP4ssw0rd;", 
        vers, 
        op => op.EnableRetryOnFailure())
);

// Add the Database Service
builder.Services.AddScoped<ZoneService>();

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();