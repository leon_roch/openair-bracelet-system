﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using OpenAir.Models;

namespace OpenAir.Data
{
    public class ZoneContext: DbContext {

        public ZoneContext(DbContextOptions options) : base(options) { }

        public DbSet<FestivalZone> FestivalZones => Set<FestivalZone>();
        public DbSet<CampingZone> CampingZones => Set<CampingZone>();
    }
}