﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using OpenAir.Data;
using OpenAir.Models;

namespace OpenAir.Services
{
    public class ZoneService
    {
        private readonly ZoneContext _context;

        public ZoneService(ZoneContext context)
        {
            _context = context;
        }
        
        /// <summary>
        /// Returns the FestivalZone with the given code
        /// </summary>
        /// <param name="code">Code of the Zone</param>
        /// <returns>The <see cref="String"/> of the Zone</returns>
        /// <exception cref="NullReferenceException">Zone with the code does not exist.</exception>
        public async Task<FestivalZone> GetFestivalZone(char code)
        {
            var zone = await _context.FestivalZones.FindAsync(code);
            if (zone is null)
            {
                throw new NullReferenceException($"The FestivalZone '{code}' doesn't exist");
            }

            return zone;
        }

        /// <summary>
        /// Creates a new FestivalZone
        /// </summary>
        /// <param name="newZone">Zone to be Created in the Database</param>
        /// <exception cref="NullReferenceException">Entity set is null.</exception>
        /// <example cref="Exception">FestivalZone with this Code already exists</example>
        public async Task CreateFestivalZone(FestivalZone newZone)
        {
            if (_context.FestivalZones == null)
            {
                throw new NullReferenceException("Entity set is null");
            }

            try
            {
                _context.FestivalZones.Add(newZone);
                await _context.SaveChangesAsync();
            }
            catch
            {
                throw new Exception($"The Code {newZone.Code} already exists");
            }
        }

        /// <summary>
        /// Removes the FestivalZone if it exists
        /// </summary>
        /// <param name="code">Code of the FestivalZone to delete</param>
        /// <exception cref="NullReferenceException">FestivalZone does not exist</exception>
        public async Task DeleteFestivalZone(char code)
        {
            var z = await GetFestivalZone(code);
            _context.FestivalZones.Remove(z);
            await _context.SaveChangesAsync();
        }

        /// <summary>
        /// Updates the name of the FestivalZone
        /// </summary>
        /// <param name="code">Code of the FestivalZone</param>
        /// <param name="name">New Name for the FestivalZone</param>
        /// <exception cref="NullReferenceException">FestivalZone with the code does not exist.</exception>
        public async Task UpdateFestivalZoneName(char code, string name)
        {
            var z = await GetFestivalZone(code);
            z.Name = name;
            await _context.SaveChangesAsync();
        }
        
        /// <summary>
        /// Returns the CampingZone with the given code
        /// </summary>
        /// <param name="code">Code of the Zone</param>
        /// <returns>The <see cref="String"/> of the Zone</returns>
        /// <exception cref="NullReferenceException">CampingZone with the code does not exist.></exception>
        public async Task<CampingZone> GetCampingZone(char code)
        {
            var zone = await _context.CampingZones.FindAsync(code);

            if (zone is null)
            {
                throw new NullReferenceException($"The CampingZone '{code}' doesn't exist");
            }

            return zone;
        }
        
        /// <summary>
        /// Creates a new CampingZone
        /// </summary>
        /// <param name="newZone">Zone to be Created in the Database</param>
        /// <exception cref="NullReferenceException">Entity set is null.</exception>
        /// <exception cref="Exception">Zone with this Code already exists</exception>
        public async Task CreateCampingZone(CampingZone newZone)
        {
            if (_context.CampingZones == null)
            {
                throw new NullReferenceException("Entity set is null");
            }

            try
            {
                _context.CampingZones.Add(newZone);
                await _context.SaveChangesAsync();
            }
            catch
            {
                throw new Exception($"The Code {newZone.Code} already exists");
            }
        }
        
        /// <summary>
        /// Removes the CampingZone if it exists
        /// </summary>
        /// <param name="code">Code of the CampingZone to delete</param>
        /// <exception cref="NullReferenceException">CampingZone does not exist</exception>
        public async Task DeleteCampingZone(char code)
        {
            var z = await GetCampingZone(code);
            _context.CampingZones.Remove(z);
            await _context.SaveChangesAsync();
        }

        /// <summary>
        /// Updates the name of the CampingZone
        /// </summary>
        /// <param name="code">Code of the CampingZone</param>
        /// <param name="name">New Name for the CampingZone</param>
        /// <exception cref="NullReferenceException">Zone with the code does not exist.</exception>
        public async Task UpdateCampingZoneName(char code, string name)
        {
            var z = await GetCampingZone(code);
            z.Name = name;
            await _context.SaveChangesAsync();
        }
    }
}