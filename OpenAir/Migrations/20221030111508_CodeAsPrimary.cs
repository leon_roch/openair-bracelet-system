﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace OpenAir.Migrations
{
    public partial class CodeAsPrimary : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_FestivalZones",
                table: "FestivalZones");

            migrationBuilder.DropPrimaryKey(
                name: "PK_CampingZones",
                table: "CampingZones");

            migrationBuilder.DropColumn(
                name: "Id",
                table: "FestivalZones");

            migrationBuilder.DropColumn(
                name: "Id",
                table: "CampingZones");

            migrationBuilder.AddPrimaryKey(
                name: "PK_FestivalZones",
                table: "FestivalZones",
                column: "Code");

            migrationBuilder.AddPrimaryKey(
                name: "PK_CampingZones",
                table: "CampingZones",
                column: "Code");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_FestivalZones",
                table: "FestivalZones");

            migrationBuilder.DropPrimaryKey(
                name: "PK_CampingZones",
                table: "CampingZones");

            migrationBuilder.AddColumn<int>(
                name: "Id",
                table: "FestivalZones",
                type: "int",
                nullable: false,
                defaultValue: 0)
                .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn);

            migrationBuilder.AddColumn<int>(
                name: "Id",
                table: "CampingZones",
                type: "int",
                nullable: false,
                defaultValue: 0)
                .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn);

            migrationBuilder.AddPrimaryKey(
                name: "PK_FestivalZones",
                table: "FestivalZones",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_CampingZones",
                table: "CampingZones",
                column: "Id");
        }
    }
}
