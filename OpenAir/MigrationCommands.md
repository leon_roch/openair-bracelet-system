# Step by Step Database migration ([source](https://learn.microsoft.com/en-us/ef/core/cli/dotnet))

## Installing the tool 
- Install dotnet tools: `dotnet tool install --global dotnet-ef` \
- If you have already installed the tools you can update them by following: \
  `dotnet tool update --global dotnet-ef`
- Add package to project: `dotnet add package Microsoft.EntityFrameworkCore.Design`
- If everything went wright the output of the command `dotnet ef` should be:

                     _/\__       
               ---==/    \\      
         ___  ___   |.    \|\    
        | __|| __|  |  )   \\\   
        | _| | _|   \_/ |  //|\\ 
        |___||_|       /   \\\/\\

## Using the tool

- ### `dotnet ef database update`
  Update the database. \
Before doing any migration this is recommended.

- ### `dotnet ef migrations add <name>` 
    Adds a new migration. \
`--context <NAME>` or `-c` The name of the `DbContext` class to generate. \
`-v` show details

- ### `dotnet ef database drop` 
    Drops the db. \
`-f` force \
`--dry-run` Show which database would be dropped, but don't drop it.