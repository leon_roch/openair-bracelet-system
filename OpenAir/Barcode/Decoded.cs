﻿namespace OpenAir.Barcode {
    public class Decoded 
    {
        public string? FestivalZone { set; get; }
        public string? CampingZone { set; get; }
        public bool OverEighteen { get; }
        public string ValidityFrom { private set; get; }
        public string ValidityUntil { private set; get; }


        public Decoded(string code) // example: vn15o18
        {
            ValidityFrom = code.Substring(2, 2);
            ValidityUntil = code.Substring(5, 2);

            OverEighteen = code[4] == 'o';
        } 
    }
}