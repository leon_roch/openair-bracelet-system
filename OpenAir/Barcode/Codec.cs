﻿using System.Text.RegularExpressions;

namespace OpenAir.Barcode
{
    public static class Codec
    {
        /// <summary>
        /// regex: <br />
        /// FestivalZone: [A-Z], <br />
        /// CampingZone: [A-Z], <br />
        /// Date from: [01-31], <br />
        /// Age: [over 18: o, under 18: u], <br />
        /// Date until: [01-31] <br />
        /// <example>VN12U15</example>
        /// </summary>
        private static readonly Regex Regex = 
            new (@"([a-z]{2})(0[1-9]|[1-2]\d|3[0-1]){1}(u|o)(0[1-9]|[1-2]\d|3[0-1]){1}", 
            RegexOptions.IgnoreCase);
        
        /// <summary>
        /// Validates the given code with the <see cref="Regex"/>
        /// </summary>
        /// <param name="code">Code to be validated</param>
        /// <returns>Whether the code is valid</returns>
        public static bool ValidCode(string code)
        {
            return code.Length == 7 && Regex.IsMatch(code);
        }
    }
}