﻿using System;
using Microsoft.AspNetCore.Mvc;
using OpenAir.Barcode;
using OpenAir.Models;
using OpenAir.Services;

namespace OpenAir.Controllers
{
    [ApiController]
    [Route("openair")]
    public class OpenAirController : Controller
    {
        private readonly ZoneService _service;
        public OpenAirController(ZoneService service)
        {
            _service = service;
        }
    
        [HttpGet("code/{code}")]
        public IActionResult Decode(string code)
        {
            if (!Codec.ValidCode(code)) return BadRequest();

            Decoded decoded = new (code);

            try
            {
                // Decoded object with getter and setter?
                // Decoded object store only zone name or code too?
                decoded.FestivalZone = _service.GetFestivalZone(code[0]).Result.Name;
                decoded.CampingZone = _service.GetCampingZone(code[1]).Result.Name;
            }
            catch (Exception e)
            {
                return NotFound(e.Message);
            }

            return Ok(decoded);
        }
        

        [HttpPost("festivalZone")]
        public IActionResult CreateFestivalZone(string name, char? code)
        {
            var zone = new FestivalZone
            {
                Name = name,
                // if code is null take first char of the name
                Code = code ?? name[0] 
            };

            // code has to be letter
            if (!char.IsLetter(zone.Code)) return BadRequest("Code has to be a letter"); 

            try
            {
                _service.CreateFestivalZone(zone).Wait();
                return CreatedAtAction(nameof(CreateFestivalZone), new {code = zone.Code}, zone);
            }
            catch (NullReferenceException e) // DBSet is null
            {
                return StatusCode(500, e.Message);
            }
            catch (Exception e) // Code already exists
            {
                return BadRequest(e.Message);
            }
        }

        
        [HttpDelete("festivalZone")]
        public IActionResult DeleteFestivalZone(char code)
        {
            // if code is null take first char of the name
            if (!char.IsLetter(code)) return BadRequest("Code has to be a letter");

            try
            {
                _service.DeleteFestivalZone(code).Wait();
                return Ok($"FestivalZone '{code}' successfully deleted");
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
        
        
        [HttpPut("festivalZone")]
        public IActionResult UpdateFestivalZone(char code, string name)
        {
            // if code is null take first char of the name
            if (!char.IsLetter(code)) return BadRequest("Code has to be a letter");
            
            try
            {
                _service.UpdateFestivalZoneName(code, name).Wait();
                return Ok(_service.GetFestivalZone(code).Result);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        
        [HttpPost("campingZone")]
        public IActionResult CreateCampingZone(string name, char? code)
        {
            var zone = new CampingZone
            {
                Name = name,
                // if code is null take first char of the name
                Code = code ?? name[0] 
            };

            // if code is null take first char of the name
            if (!char.IsLetter(zone.Code)) return BadRequest("Code has to be a letter");

            try
            {
                _service.CreateCampingZone(zone).Wait();
                return CreatedAtAction(nameof(CreateCampingZone), new {code = zone.Code}, zone);
            }
            catch (NullReferenceException e) // DBSet is null
            {
                return StatusCode(500, e.Message);
            }
            catch (Exception e) // Code already exists
            {
                return BadRequest(e.Message);
            }
        }


        [HttpDelete("campingZone")]
        public IActionResult DeleteCampingZone(char code)
        {
            // if code is null take first char of the name
            if (!char.IsLetter(code)) return BadRequest("Code has to be a letter");

            try
            {
                _service.DeleteCampingZone(code).Wait();
                return Ok($"CampingZone '{code}' successfully deleted");
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
        
        
        [HttpPut("campingZone")]
        public IActionResult UpdateCampingZone(char code, string name)
        {
            // if code is null take first char of the name
            if (!char.IsLetter(code)) return BadRequest("Code has to be a letter");
            
            try
            {
                _service.UpdateCampingZoneName(code, name).Wait();
                return Ok(_service.GetCampingZone(code).Result);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

    }
}