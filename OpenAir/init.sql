﻿DROP DATABASE IF EXISTS `openair`;
CREATE DATABASE `openair`;
USE `openair`;

ALTER DATABASE CHARACTER SET utf8mb4;


CREATE TABLE `CampingZones` (
    `Code` varchar(1) CHARACTER SET utf8mb4 NOT NULL,
    `Name` longtext CHARACTER SET utf8mb4 NOT NULL,
    CONSTRAINT `PK_CampingZones` PRIMARY KEY (`Code`)
) CHARACTER SET=utf8mb4;

INSERT INTO `CampingZones`(Code, Name)
VALUES 
 ('T', 'Tent Camping Zone'),
 ('C','Caravan Camping Zone'),
 ('V', 'VIP Camping Zone'),
 ('N', 'No Camping');
-- could be appended


CREATE TABLE `FestivalZones` (
    `Code` varchar(1) CHARACTER SET utf8mb4 NOT NULL,
    `Name` longtext CHARACTER SET utf8mb4 NOT NULL,
    CONSTRAINT `PK_FestivalZones` PRIMARY KEY (`Code`)
) CHARACTER SET=utf8mb4;

INSERT INTO `FestivalZones`(Code, Name)
VALUES
    ('N','Regular Zone'),
    ('V', 'VIP Zone'),
    ('B', 'Backstage Zone');
-- could be appended
