﻿# Openair Bracelet System

The task was to implement a concrete barcode application within three weeks. Two groups were <br />formed for this purpose. We were allowed to choose the topic ourselves. We decided to make bracelets<br /> for the visitors of an open-air, with access to different zones. We also had to implement this coding as a program.

## Installation and Setup
1. Clone this repository
2. Execute `docker-compose up` in the same folder as _docker-compose.yml_ file

## Usage
1. Execute `dotnet run` to start the application
2. Open this [url](https://localhost:7037/swagger/index.html) in any browser to see the Swagger documentation
